INSTALLING DART

To run programs written in dart on Windows download Dart SDK 

We install it using Chocolatey software, to download it run the following command in cmd as the administrator:

@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "[System.Net.ServicePointManager]::SecurityProtocol = 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"


In Command Prompt run the following command: choco install dart-sdk 

To download dependencies from pubspec.yaml file press get packages button in the top right corner of the file or run the following command in the terminal: dart pub get

We also need to intall the dart extension on vs code

RUNNING THE PROGRAM

To run the server press go to the spellcheck_api file in the bin folder and press debug which is on top of the void main. It has a post route localhost/check which expects raw text as input(Text to spellcheck).

Update dictionary is currently only a function which expects a file containing wors which will be a new dictionary.
Also file locations are absolute, to find your dictionary file search %AppData%\Microsoft\Spelling\sr-Latn-ME and update the file path in the program( use 2 slashes instaed of 1 //). Also for testing create a file and put it in the void main().


